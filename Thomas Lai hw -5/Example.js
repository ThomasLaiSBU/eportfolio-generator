       /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Page CSS Variables
var layoutTemplate;
var colorTemplate;
//Display Variables
var screenSelection;
var currentPage;
var pages;
var pTitle;
var IMG_PATH;
var ICON_PATH;
var IMG_WIDTH;
var SCALED_IMAGE_HEIGHT;
var FADE_TIME;
var SLIDESHOW_SLEEP_TIME;

// DATA FOR CURRENT SLIDE
var cssTag = window.document.styleSheets[1];
var layTag = window.document.styleSheets[0];
var titleX;
var slides;
var currentSlide;
//SlideShow Array Variables
var slidesx;
var timers = new Array();
var holds;
var tugs =0;
function Page(layoutTemplate,colorTemplate,bannerImage,title,content,footer){
    this.layout = layoutTemplate;
    this.colors = colorTemplate;
    this.banner = bannerImage;
    this.header = title;
    this.components = content;
    this.endnote =footer; 
}
function TextComp(tType,fSize,fStyle,fFamily,tHyper,tContent){
    this.type = tType;
    this.fontSize = fSize;
    this.fontStyle = fStyle;
    this.fontFamily = fFamily;
    this.hyperlinks = tHyper;
    this.content = tContent;
}
function VideoComp(fn,fp){
    this.video_file_name = fn;
    this.video_file_path = fp;
    this.content = fn;
}
function ImageComp(fn,fp){
    this.image_file_name = fn;
    this.image_file_path = fp;
    this.content = fn;
}

function TextHyperComp(content,i1,i2){
    this.url = content;
    this.index1 = i1;
    this.index2 = i2;
}
function BannerImage(fn,fp){
    this.img_fn = fn;
    this.img_fp = fp;
}
function Title(hl,fSize,fStyle,fFamily){
    this.title = hl;
    this.fontSize = fSize;
    this.fontStyle = fStyle;
    this.fontFamily = fFamily;
}
function Footer(hl,fSize,fStyle,fFamily){
    this.text = hl;
    this.fontSize = fSize;
    this.fontStyle = fStyle;
    this.fontFamily = fFamily;
    this.content = hl;
}
function contentArray(){
    
}
function Slide(initImgFile,initCaption,fontsi,fontsy,fontfa) {
    this.imgFile = initImgFile;
    this.caption = initCaption;
    this.fontSize = fontsi;
    this.fontStyle = fontsy;
    this.fontFamily = fontfa;
}
function decipherData(jsonFile){
    $.getJSON(jsonFile,function(json){
        loadProject(json);
        colorIn(pages[currentPage].colors);
        constructLayout(pages[currentPage].layout);
        
    });}
function loadProject(data){
    pTitle = data.studentName;
    for(var i = 0; i < data.page.length; i++){
        var rawPage = data.page[i];
        var tempHolder = new Array();
        for(var k = 0; k < rawPage.content.length; k++){
            var c = rawPage.content[k];
            if(c.tag === "text")
            {
                var sunshine = new Array();
                for(var u = 0; u < c.tHyper.length;u++)
                    sunshine[u] = new TextHyperComp(c.tHyper[u].link,c.tHyper[u].index,c.tHyper[u].indexEnd);
                tempHolder[k] = new TextComp(c.type,c.fontSize,c.fontStyle,c.fontFamily,sunshine,c.tContent);
            }
            else if(c.tag === "image")
            {
                tempHolder[k] = new ImageComp(c.image_file_name,c.image_path);
            }
            else if(c.tag === "video")
            {
                tempHolder[k] = new VideoComp(c.video_file_name,c.video_path);
            }
            else if(c.tag === "slideshow")
            {
                var moonshine = new Array();
                timers[k] = null;
                for(var v = 0; v < c.slides.length; v++)
                    moonshine[v] = new Slide(c.slides[v].img_fn,c.slides[v].caption,c.slides[v].fontSize,c.slides[v].fontStyle,c.slides[v].fontFamily);
                tempHolder[k] = new SlideShowComp(moonshine,timers[k],new Title(c.title.title,c.title.fontSize,c.title.fontStyle,c.title.fontFamily),0,k);
            }
        
        }
        var page = new Page(rawPage.layoutScheme,rawPage.colorScheme,new BannerImage(rawPage.bannerImage.bn_img_fn,rawPage.bannerImage.bn_img_path),new Title(rawPage.title.headline,rawPage.title.fontSize,rawPage.title.fontStyle,rawPage.title.fontFamily),tempHolder,new Footer(rawPage.footer.text,rawPage.footer.fontSize,rawPage.footer.fontStyle,rawPage.footer.fontFamily));
        pages[i] = page;
    }
    if(pages.length > 0)
        currentPage = 0;
    else
        currentPage = -1;
}
var toolbar1 = "<div id = \"toolbar1\"><p class = \"port\" id = \"newP\" >New Portfolio</p><p class = \"port\" id = \"loadP\" >Load Portfolio</p><p class = \"port\" id = \"saveP\" >Save</p> <p class = \"port\" id = \"saveAsP\">Save Portfolio As</p><p class = \"port\" id = \"exportP\">Export Portfolio</p><p class = \"port\" id = \"ex\" >Exit</p><p class = \"port\" id = \"np\" onclick = \"nextPageAction()\" >Next Page</p><p class = \"port\" id = \"cl\" onclick = \"cycleLayout()\" >Cycle Layout</p></div>";
var toolbar2 = "<div id = \"toolbar1\"><input type= \"image\" id = \"addP\" src = \"\"><input type= \"image\" id = \"selectP\" src = \"\"><input type= \"image\" id = \"removeP\" src = \"\"></p></div>";
var toolbar3 = "";
var bodyReformerA = ""+
        "<div id = \"navigation1\"></div>"+
        "<div id = \"banner\"></div>"+
        "<div id = \"navigation2\"></div>"+
        "<div id = \"bannerimg\"></div>"+
        "<div id = \"navigation3\"></div>";
function initEPortfolioGenerator(){
    IMG_PATH = "./img/";
    ICON_PATH = "./icons/";
    IMG_WIDTH = 1000;
    SCALED_IMAGE_HEIGHT = 500.0;
    FADE_TIME = 1000;
    SLIDESHOW_SLEEP_TIME = 3000;
    pages = new Array();
    var projectdata = "./example.json";
    decipherData(projectdata);
}
function constructLayout(selection){
        if(selection === "A")
            document.getElementById("cssOne").href = "example.css";
        else if(selection === "B")
            document.getElementById("cssOne").href = "example2.css";
        else if(selection === "C")
            document.getElementById("cssOne").href = "example3.css";
        else if(selection === "D")
            document.getElementById("cssOne").href = "example4.css";
        else if(selection === "E")
            document.getElementById("cssOne").href = "example5.css";
        $(this).children("div").remove();
        $("#b1").html(bodyReformerA);
        $("#bannerimg").html("<img id = \"fullard\"src = \""+pages[currentPage].banner.img_fn+"\">");
        $("#navigation1").html(toolbar1);
        $("#banner").html(pTitle+" - "+pages[currentPage].header.title);
        $("#navigation2").html(toolbar2);
        $("#navigation3").html(toolbar3);
        //var gosh3= "#navigation1{position:absolute;\ntop:10%;}";
        //layTag.insertRule(gosh3,layTag.cssRules.length);
        //gosh3= ".tComp{	width:80%;\nleft:10%;\nright:10%;\npadding-left:250px;}";
        //layTag.insertRule(gosh3,layTag.cssRules.length);
        //gosh3= ".iComp{	width:100%;left:10%;right:10%;text-align:center;}";
        //layTag.insertRule(gosh3,layTag.cssRules.length);
        //gosh3= ".vComp{	width:100%;left:10%;right:10%;text-align:center;}";
        //layTag.insertRule(gosh3,layTag.cssRules.length);
        //gosh3= ".sComp{	width:100%;left:10%;right:10%;text-align:center;}";
        //layTag.insertRule(gosh3,layTag.cssRules.length);
        
        for(var t = 0; t < pages[currentPage].components.length;t++)
        {
            if(pages[currentPage].components[t] instanceof TextComp){
                var jol = pages[currentPage].components[t];
                var changer = "";
                var recordChange = 0;
                var chil = pages[currentPage].components[t].content;
                var weird = pages[currentPage].components[t].content;
                for(var f = 0;f < jol.hyperlinks.length;f++)
                {
                    
                    var tilt = chil.substring(jol.hyperlinks[f].index1,jol.hyperlinks[f].index2);
                    if(changer === "") 
                        changer = chil.substring(0,jol.hyperlinks[f].index1+recordChange)+"<a href = \""+jol.hyperlinks[f].url+"\">"+tilt+"</a>"+chil.substring(jol.hyperlinks[f].index2+recordChange);
                    else
                        changer = changer.substring(0,jol.hyperlinks[f].index1+recordChange)+"<a href = \""+jol.hyperlinks[f].url+"\">"+tilt+"</a>"+changer.substring(jol.hyperlinks[f].index2+recordChange);
                    recordChange += (17 + jol.hyperlinks[f].url.length);
                    weird = changer;
                }
                
                if(jol.type === "paragraph")
                {
                $("#b1").append( "<div class = \"tComp\" id = \"z"+t+"\"><p id = \"c"+t+"\">"+weird+"</p></div>");
                }
                else if(jol.type === "header")
                {
                $("#b1").append( "<div class = \"tComp\" id = \"z"+t+"\"><h id = \"c"+t+"\">"+weird+"</h></div>");
                }
                else if(jol.type === "list")
                {
                    var trunt = weird.split(",");
                    var swipe = "";
                    for(var e = 0; e < trunt.length; e++)
                    {
                        var tr = "<li>"+trunt[e]+"</li>";
                        swipe += tr;
                    }
                    
                $("#b1").append( "<div class = \"tComp\" id = \"z"+t+"\"><ul id = \"c"+t+"\">"+swipe+"</ul></div>");
                }
                var gosh = "#c"+t+" { font-size:"+jol.fontSize+"px; font-family:"+jol.fontFamily+"; font-style:"+jol.fontStyle+";}";
                cssTag.insertRule(gosh,cssTag.length);
        }
            else if(pages[currentPage].components[t] instanceof ImageComp){
                 $("#b1").append( "<div class = \"iComp\" id = \"z"+t+"\"><img id = \"c"+t+"\"src =\" ./"+pages[currentPage].components[t].content+"\"></div>");
             }  
            else if(pages[currentPage].components[t] instanceof VideoComp){
                 $("#b1").append("<div class = \"vComp\" id = \"z"+t+"\"><video id = \"c"+t+"\" controls> <source src =\""+pages[currentPage].components[t].content+"\" type= \"video/mp4\"></video></div>");
             }
            else if(pages[currentPage].components[t] instanceof SlideShowComp){
                 $("#b1").append( pages[currentPage].components[t].starting());
                var gosh6 = "#slide_caption"+pages[currentPage].components[t].issue+"{ font-size:"+pages[currentPage].components[t].slides[pages[currentPage].components[t].currentSlide].fontSize+"px;font-family:"+pages[currentPage].components[t].slides[pages[currentPage].components[t].currentSlide].fontFamily+";font-style:"+pages[currentPage].components[t].slides[pages[currentPage].components[t].currentSlide].fontStyle+";}";
                cssTag.insertRule(gosh6,cssTag.cssRules.length);
                gosh6 = "#slideshow_title"+pages[currentPage].components[t].issue+"{ font-size:"+pages[currentPage].components[t].title.fontSize+"px;\nfont-family:"+pages[currentPage].components[t].title.fontFamily+";\nfont-style:"+pages[currentPage].components[t].title.fontStyle+";\n}";
                cssTag.insertRule(gosh6,cssTag.cssRules.length);
                pages[currentPage].components[t].initSlidePage();
                
                //st += "<div id = \"z"+t+"\"><p id = \"c"+t+"\">"+pages[currentPage].components[t]+"</p></div>";
            }
               else{}
        }
        $("#b1").append("<div id = \"etc\"></div>");
        $("#etc").html("<p id =\"ftr\">"+pages[currentPage].endnote.text+"</p>");
        var gosh = "#etc{ font-size:"+pages[currentPage].endnote.fontSize+"px;\nfont-family:"+pages[currentPage].endnote.fontFamily+";\nfont-style:"+pages[currentPage].endnote.fontStyle+";\n}";
        
        cssTag.insertRule(gosh,cssTag.cssRules.length);
        var gosh = "#banner{ font-size:"+pages[currentPage].header.fontSize+"px;\nfont-family:"+pages[currentPage].header.fontFamily+";\nfont-style:"+pages[currentPage].header.fontStyle+";\n}";
        
        cssTag.insertRule(gosh,cssTag.cssRules.length);
        }
function colorIn(selector){
    if(selector === "A")
        document.getElementById("cssTwo").href = "exampleColor.css";
    else if (selector === "B")
        document.getElementById("cssTwo").href = "exampleColor2.css";
    else if (selector === "C")
        document.getElementById("cssTwo").href = "exampleColor3.css";
    else if (selector === "D")
        document.getElementById("cssTwo").href = "exampleColor4.css";
    else if (selector === "E")
        document.getElementById("cssTwo").href = "exampleColor5.css";
        
            
 }

    //===================================================================
function processPreviousRequest1(just){
    pages[currentPage].components[just].processPreviousRequest();
}
function processPlayPauseRequest1(just){
    pages[currentPage].components[just].processPlayPauseRequest();
}
function processNextRequest1(just){
    pages[currentPage].components[just].processNextRequest();
}
function SlideShowComp(ss,clock,name,curr,num){
    this.slides = ss;
    this.timer = clock;
    this.title = name;
    this.currentSlide = curr;
    this.issue = num;
    this.autoScaleImage = function()  {
	var origHeight = $("#slide_img"+this.issue+"").height();
	var scaleFactor = SCALED_IMAGE_HEIGHT/origHeight;
	var origWidth = $("#slide_img"+this.issue+"").width();
	var scaledWidth = origWidth * scaleFactor;
	$("#slide_img"+this.issue+"").height(SCALED_IMAGE_HEIGHT);
	$("#slide_img"+this.issue+"").width(scaledWidth);
	var left = (IMG_WIDTH-scaledWidth)/2;
	$("#slide_img"+this.issue+"").css("left", left);
};
this.fadeInCurrentSlide= function() {
    var filePath = IMG_PATH + this.slides[this.currentSlide].imgFile;
    var safety = this;
    var gosh = "#slide_caption"+safety.issue+"{ font-size:"+safety.fontSize+"px;\nfont-family:"+safety.slides[safety.currentSlide].fontFamily+";\nfont-style:"+safety.slides[safety.currentSlide].fontStyle+";\n}";
                cssTag.insertRule(gosh,cssTag.cssRules.length);
    $("#slide_img"+this.issue+"").fadeOut(FADE_TIME, function(){
	$(this).attr("src", filePath).bind('onreadystatechange load', function(){
	    if (this.complete) {
                $(this).fadeIn(FADE_TIME);
		$("#slide_caption"+safety.issue+"").html(safety.slides[safety.currentSlide].caption);
		safety.autoScaleImage();
	    }
	});
  });     
};
this.processPreviousRequest = function() {
    this.currentSlide--;
    if (this.currentSlide < 0)
	this.currentSlide = this.slides.length-1;
   this.fadeInCurrentSlide();
};
this.processPlayPauseRequest = function() {
    var safety = this;
    if (timers[this.issue] === null) {
        
	timers[safety.issue] = setInterval(function(){processNextRequest1(safety.issue);}, SLIDESHOW_SLEEP_TIME);
	$("#play_pause_button"+safety.issue+"").attr("src", ICON_PATH + "Pause.png");
    }
  else {
	clearInterval(timers[safety.issue]);
	timers[safety.issue] = null;
	$("#play_pause_button" + safety.issue + "").attr("src", ICON_PATH + "Play.png");
    }	
};
this.processNextRequest = function() {
    this.currentSlide++;
    if (this.currentSlide >= this.slides.length)
	this.currentSlide = 0;
    this.fadeInCurrentSlide();
};
this.initSlidePage = function() {
    $("#slideshow_title"+this.issue).html(this.title.title);
    
if (this.currentSlide >= 0) {
	$("#slide_caption"+this.issue).html(this.slides[this.currentSlide].caption);

	$("#slide_img"+this.issue+"").attr("src", IMG_PATH + this.slides[this.currentSlide].imgFile);

	this.autoScaleImage();

    }
};
this.starting = function(){ 
                return "<div class = \"sComp\" id = \"z"+this.issue+"\"><div id=\"slideshow_title"+this.issue+"\"></div>"+
	    "<img class = \"sComp\" id=\"slide_img"+this.issue+"\" />"+
	    "<div class = \"sComp\" id=\"slide_caption"+this.issue+"\"></div>"+
	    "<div class = \"sComp\" id=\"slideshow_controls"+this.issue+"\">"+
		"<input class = \"ctrls\" id=\"previous_button"+this.issue+"\" type=\"image\" src=\"./icons/Previous.png\" onclick=\"processPreviousRequest1("+this.issue+")\">"+
		"<input class = \"ctrls\" id=\"play_pause_button"+this.issue+"\" type=\"image\" src=\"./icons/Play.png\" onclick=\"processPlayPauseRequest1("+this.issue+")\">"+
		"<input class = \"ctrls\" id=\"next_button"+this.issue+"\" type=\"image\" src=\"./icons/Next.png\" onclick=\"processNextRequest1("+this.issue+");\"></div>";
           
};
    
}
function nextPageAction(){
    if(currentPage < pages.length-1)
        currentPage++;
    else
        currentPage = 0;
    $("#b1").html(bodyReformerA);
    colorIn(pages[currentPage].colors);
    constructLayout(pages[currentPage].layout);
    
    
}
function cycleLayout(){
    var layouts = new Array();
   layouts = ["A","B","C","D","E"];
    if(tugs < layouts.length-1)
        tugs += 1;
    else
        tugs =0;
    
    
    $(this).children("div").remove();
    $("#b1").html(bodyReformerA);
    constructLayout(layouts[tugs]);
}