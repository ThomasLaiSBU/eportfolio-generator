/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

/**
 *
 * @author Thomas
 */
public class Constants {
    public static String ICON_PATH = "icons/";
    public static String NAME_FN = "Namer.png";
    public static String NAME_FN_D = "Enter Portfolio Owner Name";
    public static String LAYOUT_FN = "Layout.png";
    public static String COLOR_FN = "Color.png";
    public static String BANNER_FN = "Banner.png";
    public static String FOOTER_FN = "Footer.png";
    public static String TEXT_FN = "Text.png";
    public static String IMG_FN = "Image.png";
    public static String VID_FN = "Video.png";
    public static String SS_FN = "Slideshow.png";
    public static String EDIT_COMP = "EditC.png";
    public static String REMOVE_COMP = "RemoveCo.png";
    public static String PAGE_TITLE = "PTitle.png";
    
    public static String LAYOUT_FN_D = "Selects the Layout for the current Page";
    public static String COLOR_FN_D = "Selects the Color for the current Page";
    public static String BANNER_FN_D = "Choose a Banner Image for the Current Page";
    public static String FOOTER_FN_D = "Edit the Footer of this Page";
    public static String TEXT_FN_D = "Add a Text Component onto this Page";
    public static String IMG_FN_D = "Add an Image Component onto this Page";
    public static String VID_FN_D = "Add a Video Component onto this Page";
    public static String SS_FN_D = "Add a Slideshow Component onto this Page";
    public static String EDIT_COMP_D = "Edit the Selected Component";
    public static String REMOVE_COMP_D = "Remove the Selected Component";
    public static String PAGE_TITLE_D = "Edit the current Page's title";
    
    public static String CSS_EDITOR_TOOLBAR = "editor_toolbar";
    
    public static String NP_ICON = "Add.png";
    public static String LP_ICON = "Load.png";
    public static String SP_ICON = "Save.png";
    public static String SAP_ICON = "EditScheduleItem.png";
    public static String EP_ICON = "Export.png";
    public static String EXIT_ICON = "Exit.png";
    
    public static String NP_ICON_D = "Create a New ePorfolio";
    public static String LP_ICON_D = "Load an existing ePortfolio";
    public static String SP_ICON_D = "Save current ePortfolio";
    public static String SAP_ICON_D = "Save current ePortfolio as...";
    public static String EP_ICON_D = "Export Portfolio";
    public static String EXIT_ICON_D = "Exit Application";
    
    public static String CSS_FILE_TOOLBAR = "file_toolbar";
    
    public static String NPAGE_ICON = "Npage.png";
    public static String RPAGE_ICON = "Rpage.png";
    public static String SELECT_PAGE_ICON = "SPage.png";
    
    public static String NPAGE_ICON_D = "Create a New Page";
    public static String RPAGE_ICON_D = "Remove current Page";
    public static String SELECT_PAGE_ICON_D = "Select Page";
    
    public static String CSS_SITE_TOOLBAR = "site_toolbar";
    public static String CC_EDIT = "edit";
}
