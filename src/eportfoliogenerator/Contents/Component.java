/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.Contents;

/**
 *
 * @author Thomas
 */
public class Component {
    String type;
    String[] content;
    public Component(String t, String[] c){
        type = t;
        content = c;
    }
    public String getType(){
        return type;
    }
    public String[] getContent()
    {
        return content;
    }
}
