/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.Contents;

import javafx.scene.text.Font;

/**
 *
 * @author Thomas
 */
public class Slide {
    String caption;
    String imageFileName;
    Font fonts;
    public Slide()
    {
        imageFileName = "icon";
        caption = "";
    }
    public Slide(String img, String cap,Font phont)
    {
        imageFileName = img;
        caption = cap;
        fonts = phont;
    }
}
