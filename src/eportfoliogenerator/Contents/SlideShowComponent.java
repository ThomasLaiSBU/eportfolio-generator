/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.Contents;

import java.util.ArrayList;
import javafx.scene.text.Font;

/**
 *
 * @author Thomas
 */
public class SlideShowComponent extends Component{
    String ssTitle;
    Font fonts;
    String contents[];
    ArrayList<Slide> slides;
    public SlideShowComponent(String t, String[] c) {
        super(t, c);
    }
    public SlideShowComponent(String t, String[] c,ArrayList<Slide> s,Font f)
    {
        super(t,c);
        ssTitle = c[0];
        slides = s;
        fonts = f;
    }
    
}
