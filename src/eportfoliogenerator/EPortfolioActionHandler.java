/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import static eportfoliogenerator.Constants.BANNER_FN_D;
import eportfoliogenerator.Contents.Component;
import eportfoliogenerator.Contents.Slide;
import eportfoliogenerator.Contents.SlideShowComponent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Thomas
 */
class EPortfolioActionHandler {
    Portfolio champ;
    EPortfolioModelView ui;
    File selectedFile;
    Font uniFont;
    ArrayList<Slide> uniSlide;
    ArrayList<String> s1 = new ArrayList<String>();
    String selectedText;
    String selectedUrl;
    public EPortfolioActionHandler(Portfolio chump , EPortfolioModelView uy)
    {
        champ = chump;
        ui = uy;
        selectedFile = null;
        uniFont = new Font("Times New Roman", 15);
        uniSlide = new ArrayList<>();
        selectedText  = "";
        selectedUrl = "";
    }
    public void addPage(){
    Page newpage = new Page(champ);
    champ.addPage(newpage);
    ui.updateToolbarControls(false);
    ui.loadPage(champ.getSelectedPage());
    }
    public void removePage(){
    champ.removeSelectedPage();
    ui.updateToolbarControls(false);
    ui.loadPage(champ.getSelectedPage());
    }
    public void handleName() {
        //Stage
        Stage dialog = new Stage();
        dialog.setHeight(200);
        dialog.setWidth(300);
        //Content
        Label fileLabel = new Label("Profile Name:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(champ.getName());
        
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
        po.getStyleClass().add("distort");
        //Button(Confirms)
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            champ.setName(op.getText());
            dialog.close();
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dialog.close();
        });
        
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        //ButtonPanel
        HBox buttonSpot = new HBox(5);
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //ContentPanel
        VBox contentPanel = new VBox(5);
        contentPanel.setAlignment(Pos.CENTER);
        contentPanel.getChildren().add(po);
        contentPanel.getChildren().add(fontChanger);
        contentPanel.getStyleClass().add("small");
        //ContentLayout
        BorderPane content = new BorderPane();
        content.setCenter(contentPanel);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        //Scene
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        //ShowStage
        dialog.setScene(gop);
        dialog.showAndWait();
       //@TODO set bOK action to change footer
    }
    public void handleLayout() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        //RawContent
        Label op = new Label("Select Layout:");
        op.setFont(new Font("Arial", 20));
        
        ObservableList<String> lChoiceList = FXCollections.<String>observableArrayList();
        lChoiceList.addAll(Arrays.asList(ui.getLayout()));
        ChoiceBox layoutChoices = new ChoiceBox(lChoiceList);
        
        //Content
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(layoutChoices);
        po.getStyleClass().add("husk");
        
        //Buttons(Confirm)
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            champ.getSelectedPage().layout = layoutChoices.getSelectionModel().getSelectedItem().toString();
            dia.close();
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        bOK.setAlignment(Pos.BOTTOM_CENTER);
        bCancel.setAlignment(Pos.BOTTOM_CENTER);
        
        HBox buttonSpot = new HBox(5);
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        //ContentLayout
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        
        //Scene
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        
        //ShowStage
        dia.setScene(gop);
        dia.showAndWait();
        //@TODO : set bOK activation to implement layoutChoices, set bCancel to cancel
    }

    public void handleColor() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        Label op = new Label("Select Color:");
        op.setFont(new Font("Arial", 20));
        
        ObservableList<String> lChoiceList = FXCollections.<String>observableArrayList();
        lChoiceList.addAll(Arrays.asList(ui.getColor()));
        ChoiceBox colorChoices = new ChoiceBox(lChoiceList);
        
        
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(colorChoices);
        po.getStyleClass().add("distort");
        //Buttons
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            champ.getSelectedPage().color = colorChoices.getSelectionModel().getSelectedItem().toString();
            dia.close();
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        //ContentLayout
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        
        dia.setScene(gop);
        dia.showAndWait();
        //@TODO : set bOK activation to implement colorChoices, set bCancel to cancel
    }

    public void handleBanner() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        //Content
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        
        Button op = new Button("Select Banner Image");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        
        
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        
        //Buttons
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            champ.getSelectedPage().banner = selectedFile;
            dia.close();
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        //ButtonPanel
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        //ContentLayout
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set image implementation
    }

    public void handleFooter() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        Label fileLabel = new Label("Footer:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(champ.getSelectedPage().footer);
        
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
        po.getStyleClass().add("distort");
        
        //buttons
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            champ.getSelectedPage().footer = op.getText();
            champ.getSelectedPage().footerFont = uniFont;
            dia.close();
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        
        //ButtonPanel
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        //ContentBPanel
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getChildren().add(fontChanger);
        content.getStyleClass().add("small");
        
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }

    public void handleAddText() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        
        Label op = new Label("Select Text Type:");
        op.setFont(new Font("Arial", 20));
        
        ObservableList<String> lChoiceList = FXCollections.<String>observableArrayList();
        lChoiceList.addAll(Arrays.asList(ui.textTypes));
        ChoiceBox textChoices = new ChoiceBox(lChoiceList);
        
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(textChoices);
        po.getStyleClass().add("distort");
        
        
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            createTextType(textChoices.getValue().toString());
        });
        
        Button bCancel = new Button("Cancel");
         bCancel.setOnAction(e -> {
            dia.close();
        });
         
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
       
        
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        
        dia.setScene(gop);
        dia.show();
    }

    public void handleAddImage() {
        Stage dia = new Stage();
        dia.setHeight(300);
        dia.setWidth(400);
        
        
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        
        Button op = new Button("Select Image");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        
        Label filesLabel = new Label("Caption:");
        filesLabel.setFont(new Font("Arial", 20));
        
        TextField op1 = new TextField();
        
        Label fileLabel1 = new Label("Height:");
        fileLabel1.setFont(new Font("Arial", 15));
        
        TextField op2 = new TextField();
        
        Label fileLabel2 = new Label("Width:");
        fileLabel2.setFont(new Font("Arial", 15));
        
        TextField op3 = new TextField();
        
        ObservableList<String> lChoiceList = FXCollections.<String>observableArrayList();
        String[] choices = {"No Float","Left Float","Right Float"};
        lChoiceList.addAll(Arrays.asList(choices));
        ChoiceBox floatChoices = new ChoiceBox(lChoiceList);
        
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel1);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        
        HBox po3 = new HBox();
        po3.getChildren().add(fileLabel2);
        po3.getChildren().add(op3);
        po3.getStyleClass().add("distort");
        
        HBox po1 = new HBox();
        po1.getChildren().add(filesLabel);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        
        HBox po4 = new HBox();
        po4.getChildren().add(floatChoices);
        po4.getStyleClass().add("distort");
        
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cla = {"image", selectedFile.getName(), op1.getText(), op2.getText(), op3.getText(), floatChoices.getValue().toString()};
            champ.getSelectedPage().addComponent("image",cla,uniFont);
            ui.showPage();
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getChildren().add(po4);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        
        BorderPane content = new BorderPane();
        content.setCenter(co);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set image implementation
    }

    public void handleAddVideo() {
        Stage dia = new Stage();
        dia.setHeight(300);
        dia.setWidth(400);
        
        
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        
        Button op = new Button("Select Video");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        
        Label filesLabel = new Label("Caption:");
        filesLabel.setFont(new Font("Arial", 20));
        
        TextField op1 = new TextField();
        
        Label fileLabel1 = new Label("Height:");
        fileLabel1.setFont(new Font("Arial", 15));
        
        TextField op2 = new TextField();
        
        Label fileLabel2 = new Label("Width:");
        fileLabel2.setFont(new Font("Arial", 15));
        
        TextField op3 = new TextField();
        
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel1);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        
        HBox po3 = new HBox();
        po3.getChildren().add(fileLabel2);
        po3.getChildren().add(op3);
        po3.getStyleClass().add("distort");
        
        HBox po1 = new HBox();
        po1.getChildren().add(filesLabel);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        
        
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cla = {"video", selectedFile.getName(),op1.getText(),op2.getText(),op3.getText()};
            champ.getSelectedPage().addComponent("video",cla,uniFont);
            ui.showPage();
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        
        BorderPane content = new BorderPane();
        content.setCenter(co);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set image implementation
    }

    public void handleAddSS() {
        Stage dia = new Stage();
        uniSlide = new ArrayList<Slide>(0);
        uniFont = new Font("Times New Roman", 15);
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label titleLabel = new Label("Enter Slideshow Title:");
        titleLabel.setFont(new Font("Arial", 20));
        TextField titleAcceptor = new TextField();
        Label fileLabel = new Label("Slides created:");
        fileLabel.setFont(new Font("Arial", 20));
        Label filesLabel = new Label("");
        filesLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Add Slide");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    addSlide(fileLabel,dia,filesLabel);
	});
        
        HBox po = new HBox();
        po.getChildren().add(titleLabel);
        po.getChildren().add(titleAcceptor);
        po.getStyleClass().add("distort");
        //@TODO
        VBox po1 = new VBox();
        po1.getChildren().add(op);
        po1.getChildren().add(fileLabel);
        po1.getChildren().add(filesLabel);
        po1.getStyleClass().add("distort");
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            String[] cla = {"slideshow", titleAcceptor.getText()};
            champ.getSelectedPage().addSlideShow("slideshow",cla,uniSlide,uniFont);
            ui.showPage();
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set image implementation
    }

    public void handleEdit() {
        switch (champ.getSelectedPage().selected.type) {
            case "text":
                String x = champ.getSelectedPage().selected.content[0];
                uniFont = champ.getSelectedPage().selected.font;
                switch (x) {
                    case "header":
                    {
                        Stage dia = new Stage();
                        dia.setHeight(200);
                        dia.setWidth(300);
                        BorderPane content = new BorderPane();
                        Label fileLabel = new Label("Header:");
                        fileLabel.setFont(new Font("Arial", 20));
                        TextField op = new TextField(champ.getSelectedPage().selected.content[1]);
                        FlowPane buttonSpot = new FlowPane();
                        HBox po = new HBox();
                        po.getChildren().add(fileLabel);
                        po.getChildren().add(op);
                        po.getStyleClass().add("distort");
                        //@TODO
                        Button fontChanger = new Button("Set Font");
                        fontChanger.setOnAction(e -> {
                            setFont();
                        });             Button bOK = new Button("OK");
                        bOK.setOnAction(e -> {
                            dia.close();
                            String[] cus = {"header",op.getText()};
                            champ.getSelectedPage().selected.content = cus;
                            champ.getSelectedPage().selected.font = uniFont;
                            ui.showPage();
                        });             Button bCancel = new Button("Cancel");
                        buttonSpot.getChildren().add(bOK);
                        buttonSpot.getChildren().add(bCancel);
                        buttonSpot.getStyleClass().add("confirm");
                        //@TODO
                        bCancel.setOnAction(e -> {
                            dia.close();
                        });             VBox co = new VBox(10);
                        co.getChildren().add(po);
                        co.getChildren().add(fontChanger);
                        co.getStyleClass().add("distort");
                        content.setCenter(co);
                        content.setBottom(buttonSpot);
                        content.getStyleClass().add("small");
                        Scene gop = new Scene(content);
                        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
                        dia.setScene(gop);
                        dia.showAndWait();
                        break;
                    }
                    case "paragraph":
                    {
                        Stage dia = new Stage();
                        dia.setHeight(200);
                        dia.setWidth(300);
                        BorderPane content = new BorderPane();
                        Label fileLabel = new Label("Paragraph:");
                        fileLabel.setFont(new Font("Arial", 20));
                        TextField op = new TextField(champ.getSelectedPage().selected.content[1]);
                        FlowPane buttonSpot = new FlowPane();
                        HBox po = new HBox();
                        po.getChildren().add(fileLabel);
                        po.getChildren().add(op);
                        po.getStyleClass().add("distort");
                        //@TODO
                        Button hyper = new Button("Add HyperLink");
                        hyper.setOnAction(e -> {
                            addHyperLink();
                        });             Button fontChanger = new Button("Set Font");
                        fontChanger.setOnAction(e -> {
                            setFont();
                        });             Button bOK = new Button("OK");
                        bOK.setOnAction(e -> {
                            dia.close();
                            String[] cus = {"paragraph",op.getText()};
                            champ.getSelectedPage().selected.content = cus;
                            champ.getSelectedPage().selected.font = uniFont;
                            ui.showPage();
                        });             Button bCancel = new Button("Cancel");
                        buttonSpot.getChildren().add(bOK);
                        buttonSpot.getChildren().add(bCancel);
                        buttonSpot.getStyleClass().add("confirm");
                        //@TODO
                        bCancel.setOnAction(e -> {
                            dia.close();
                        });             VBox co = new VBox(10);
                        co.getChildren().add(po);
                        co.getChildren().add(hyper);
                        co.getChildren().add(fontChanger);
                        co.getStyleClass().add("distort");
                        content.setCenter(co);
                        content.setBottom(buttonSpot);
                        content.getStyleClass().add("small");
                        Scene gop = new Scene(content);
                        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
                        dia.setScene(gop);
                        dia.showAndWait();
                        break;
                    }
                    case "list":
                    {
                        int counts = 0;
                        s1 = new ArrayList<String>();
                        String[] pop = champ.getSelectedPage().selected.content;
                        for(String uio: pop)
                            s1.add(uio);s1.remove(0);
                            String listOf = "";
                            for (String s11 : s1) {
                                listOf += s11 + "\n";
                            }
                            Stage dia = new Stage();
                            dia.setHeight(200);
                            dia.setWidth(300);
                            BorderPane content = new BorderPane();
                            Label fileLabel = new Label("Items:"+ listOf);
                            fileLabel.setFont(new Font("Arial", 20));
                            Button op = new Button("Add Item");
                            op.setOnAction(e -> {
                                addItem(counts,fileLabel);
                                
                            });             ChoiceBox itemListing = new ChoiceBox();
                            Button remover = new Button("Remove");
                            FlowPane buttonSpot = new FlowPane();
                            VBox po = new VBox();
                            po.getChildren().add(op);
                            po.getChildren().add(fileLabel);
                            po.getStyleClass().add("distort");
                            HBox po2 = new HBox(3);
                            po2.getChildren().add(remover);
                            po2.getChildren().add(itemListing);
                            Button bOK = new Button("OK");
                            bOK.setOnAction(e -> {
                                
                                s1.add(0,"list");
                                String[] sto = new String[s1.size()];
                                sto = s1.toArray(sto);
                                dia.close();
                                champ.getSelectedPage().selected.content = sto;
                                champ.getSelectedPage().selected.font = uniFont;
                                ui.showPage();
                            });             Button bCancel = new Button("Cancel");
                            buttonSpot.getChildren().add(bOK);
                            buttonSpot.getChildren().add(bCancel);
                            buttonSpot.getStyleClass().add("confirm");
                            bCancel.setOnAction(e -> {
                                dia.close();
                            });             //HBox fooo = new HBox(5);
                            //fooo.getChildren().add(po);
                            //fooo.getChildren().add(po2);
                            VBox co = new VBox(10);
                            co.getChildren().add(po);
                            co.getChildren().add(po2);
                            co.setPrefHeight(300);
                            co.getStyleClass().add("distort");
                            content.setCenter(co);
                            content.setBottom(buttonSpot);
                            content.getStyleClass().add("small");
                            content.setPrefHeight(400);
                            Scene gop = new Scene(content,400,300);
                            gop.getStylesheets().add("eportfoliogenerator/style/style.css");
                            dia.setScene(gop);
                            dia.setHeight(500);
                            dia.setHeight(400);
                            dia.showAndWait();
                            break;
                    }
                }   break;
            case "image":
            {
                uniFont = champ.getSelectedPage().selected.font;
                Stage dia = new Stage();
                dia.setHeight(300);
                dia.setWidth(400);
                BorderPane content = new BorderPane();
                Label fileLabel = new Label("Selected File: " +  champ.getSelectedPage().selected.content[1]);
                fileLabel.setFont(new Font("Arial", 20));
                Button op = new Button("Select Image");
                op.setTooltip(new Tooltip(BANNER_FN_D));
                op.setOnAction(e -> {
                    setFiles(fileLabel,dia);
                });         Label filesLabel = new Label("Caption:");
                filesLabel.setFont(new Font("Arial", 20));
                TextField op1 = new TextField(champ.getSelectedPage().selected.content[2]);
                Label fileLabel1 = new Label("Height:" );
                fileLabel1.setFont(new Font("Arial", 15));
                TextField op2 = new TextField(champ.getSelectedPage().selected.content[3]);
                Label fileLabel2 = new Label("Width:");
                fileLabel2.setFont(new Font("Arial", 15));
                TextField op3 = new TextField(champ.getSelectedPage().selected.content[4]);
                HBox po2 = new HBox();
                po2.getChildren().add(fileLabel1);
                po2.getChildren().add(op2);
                po2.getStyleClass().add("distort");
                HBox po3 = new HBox();
                po3.getChildren().add(fileLabel2);
                po3.getChildren().add(op3);
                po3.getStyleClass().add("distort");
                HBox po1 = new HBox();
                po1.getChildren().add(filesLabel);
                po1.getChildren().add(op1);
                po1.getStyleClass().add("distort");
                FlowPane buttonSpot = new FlowPane();
                HBox po = new HBox();
                    po.getChildren().add(op);
                    po.getChildren().add(fileLabel);
                    po.getStyleClass().add("distort");
                    //@TODO
                    Button bOK = new Button("OK");
                    bOK.setOnAction(e -> {
                        dia.close();
                        String[] cla = {"image", selectedFile.getName(),op1.getText(),op2.getText(),op3.getText()};
                        champ.getSelectedPage().selected.content = cla;
                        ui.showPage();
                    });         Button bCancel = new Button("Cancel");
                    buttonSpot.getChildren().add(bOK);
                    buttonSpot.getChildren().add(bCancel);
                    buttonSpot.getStyleClass().add("confirm");
                    Button fontChanger = new Button("Set Font");
                    //@TODO
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
                    content.setBottom(buttonSpot);
                    fontChanger.setOnAction(e -> {
                        setFont();
                    });         bCancel.setOnAction(e -> {
                        dia.close();
                    });         content.getStyleClass().add("small");
                    Scene gop = new Scene(content);
                    gop.getStylesheets().add("eportfoliogenerator/style/style.css");
                    dia.setScene(gop);
                    dia.showAndWait();
                    //@TODO set image implementation
                    break;
            }
            case "video":
            {
                    Stage dia = new Stage();
                    dia.setHeight(300);
                    dia.setWidth(400);
                    BorderPane content = new BorderPane();
                    Label fileLabel = new Label("Selected File: " + champ.getSelectedPage().selected.content[1]);
                    fileLabel.setFont(new Font("Arial", 20));
                    Button op = new Button("Select Video");
                    op.setTooltip(new Tooltip(BANNER_FN_D));
                    op.setOnAction(e -> {
                        setFiles(fileLabel,dia);
                    });         Label filesLabel = new Label("Caption:");
                    filesLabel.setFont(new Font("Arial", 20));
                    TextField op1 = new TextField(champ.getSelectedPage().selected.content[2]);
                    Label fileLabel1 = new Label("Height:");
                    fileLabel1.setFont(new Font("Arial", 15));
                    TextField op2 = new TextField(champ.getSelectedPage().selected.content[3]);
                    Label fileLabel2 = new Label("Width:");
                    fileLabel2.setFont(new Font("Arial", 15));
                    TextField op3 = new TextField(champ.getSelectedPage().selected.content[4]);
                    HBox po2 = new HBox();
                    po2.getChildren().add(fileLabel1);
                    po2.getChildren().add(op2);
                    po2.getStyleClass().add("distort");
                    HBox po3 = new HBox();
                    po3.getChildren().add(fileLabel2);
                    po3.getChildren().add(op3);
                    po3.getStyleClass().add("distort");
                    HBox po1 = new HBox();
                    po1.getChildren().add(filesLabel);
                    po1.getChildren().add(op1);
                    po1.getStyleClass().add("distort");
                    FlowPane buttonSpot = new FlowPane();
                    HBox po = new HBox();
                    po.getChildren().add(op);
                    po.getChildren().add(fileLabel);
                    po.getStyleClass().add("distort");
                    //@TODO
                    Button bOK = new Button("OK");
                    bOK.setOnAction(e -> {
                        dia.close();
                        String[] cla = {"video", selectedFile.getName(),op1.getText(),op2.getText(),op3.getText()};
                        champ.getSelectedPage().addComponent("video",cla,uniFont);
                        ui.showPage();
                        
                    });         Button bCancel = new Button("Cancel");
                    buttonSpot.getChildren().add(bOK);
                    buttonSpot.getChildren().add(bCancel);
                    buttonSpot.getStyleClass().add("confirm");
                    Button fontChanger = new Button("Set Font");
                    //@TODO
                    VBox co = new VBox(10);
                    co.getChildren().add(po);
                    co.getChildren().add(po1);
                    co.getChildren().add(po2);
                    co.getChildren().add(po3);
                    co.getChildren().add(fontChanger);
                    co.getStyleClass().add("distort");
                    content.setCenter(co);
                    content.setBottom(buttonSpot);
                    fontChanger.setOnAction(e -> {
                        setFont();
                    });         bCancel.setOnAction(e -> {
                        dia.close();
                    });         content.getStyleClass().add("small");
                    Scene gop = new Scene(content);
                    gop.getStylesheets().add("eportfoliogenerator/style/style.css");
                    dia.setScene(gop);
                    dia.showAndWait();
                    //@TODO set image implementation
                    break;
            }
            case "slideshow":
            {
                uniSlide = ((SlideShowComponent)champ.getSelectedPage().selected).slides;
                Stage dia = new Stage();
                    uniSlide = new ArrayList<Slide>(0);
                    uniFont = champ.getSelectedPage().selected.font;
                    dia.setHeight(200);
                    dia.setWidth(300);
                    BorderPane content = new BorderPane();
                    Label titleLabel = new Label("Enter Slideshow Title:");
                    titleLabel.setFont(new Font("Arial", 20));
                    TextField titleAcceptor = new TextField(champ.getSelectedPage().selected.content[1]);
                    String peer = "";
                    for (Slide slide : ((SlideShowComponent)champ.getSelectedPage().selected).slides) {
                        peer += slide.imageFileName + "\n";
                    }       Label fileLabel = new Label("Slides created:" + peer);
                    fileLabel.setFont(new Font("Arial", 20));
                    Label filesLabel = new Label("");
                    filesLabel.setFont(new Font("Arial", 20));
                    Button op = new Button("Add Slide");
                    op.setTooltip(new Tooltip(BANNER_FN_D));
                    op.setOnAction(e -> {
                        addSlide(fileLabel,dia,filesLabel);
                    });         FlowPane buttonSpot = new FlowPane();
                    HBox po = new HBox();
                    po.getChildren().add(titleLabel);
                    po.getChildren().add(titleAcceptor);
                    po.getStyleClass().add("distort");
                    //@TODO
                    VBox po1 = new VBox();
                    po1.getChildren().add(op);
                    po1.getChildren().add(fileLabel);
                    po1.getChildren().add(filesLabel);
                    po1.getStyleClass().add("distort");
                    Button bOK = new Button("OK");
                    bOK.setOnAction(e -> {
                        String[] cla = {"slideshow", titleAcceptor.getText()};
            
                        ((SlideShowComponent)champ.getSelectedPage().selected).slides = uniSlide;
                        ((SlideShowComponent)champ.getSelectedPage().selected).contents = cla;
                        ((SlideShowComponent)champ.getSelectedPage().selected).fonts = uniFont;
                        ui.showPage();
                        dia.close();
        });         Button bCancel = new Button("Cancel");
                    buttonSpot.getChildren().add(bOK);
                    buttonSpot.getChildren().add(bCancel);
                    buttonSpot.getStyleClass().add("confirm");
                    //@TODO
        VBox co = new VBox(10);
                    co.getChildren().add(po);
                    co.getChildren().add(po1);
                    content.setCenter(co);
                    content.setBottom(buttonSpot);
                    bCancel.setOnAction(e -> {
            dia.close();
        });         content.getStyleClass().add("small");
                    Scene gop = new Scene(content);
                    gop.getStylesheets().add("eportfoliogenerator/style/style.css");
                    dia.setScene(gop);
                    dia.showAndWait();
                    break;
                }
        }
    }

    public void handleRemove() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        
        Label fileLabel = new Label("Remove Selected Component?");
        fileLabel.setFont(new Font("Arial", 20));
        
        
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
         po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            int poy = champ.getSelectedPage().content.indexOf(champ.getSelectedPage().selected);
            champ.getSelectedPage().content.remove(poy);
            champ.getSelectedPage().labelList.remove(poy);
            champ.getSelectedPage().selected = null;
            ui.showPage();
           
        });
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
       Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }

    public void handleTitle() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Page Title:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.title*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
        Button fontChanger = new Button("Set Font");
        
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            champ.getSelectedPage().title = op.getText();
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }

    public void setFiles(Label unknowing,Stage known) {
        FileChooser slideShowFileChooser = new FileChooser();
        selectedFile = slideShowFileChooser.showOpenDialog(known);
        unknowing.setText(selectedFile.getName());
    }
    public void setFilesSlide(Label unknowing,Stage known,Label beq) {
        FileChooser slideShowFileChooser = new FileChooser();
        selectedFile = slideShowFileChooser.showOpenDialog(known);
        
        
    }
    public void addSlide(Label unknowing,Stage known,Label beq)
    {
        Stage dia = new Stage();
        dia.setHeight(300);
        dia.setWidth(400);
        selectedFile = null;
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Select Image");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
            beq.setText(beq.getText() +"\n"+selectedFile.getName());
	});
        Label filesLabel = new Label("Caption:");
        filesLabel.setFont(new Font("Arial", 20));
        TextField op1 = new TextField();
        FlowPane buttonSpot = new FlowPane();
        Label fileLabel1 = new Label("Height:");
        fileLabel1.setFont(new Font("Arial", 15));
        TextField op2 = new TextField();
        Label fileLabel2 = new Label("Width:");
        fileLabel2.setFont(new Font("Arial", 15));
        TextField op3 = new TextField();
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel1);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        HBox po3 = new HBox();
        po3.getChildren().add(fileLabel2);
        po3.getChildren().add(op3);
        po3.getStyleClass().add("distort");
        HBox po1 = new HBox();
        po1.getChildren().add(filesLabel);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            uniSlide.add(new Slide(selectedFile.getName(),op1.getText(),op2.getText(),op3.getText(),uniFont));
        });
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getStyleClass().add("distort");
        co.getChildren().add(fontChanger);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set image implementation
    }
    public void setFont()
    {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Font Size: ");
        fileLabel.setFont(new Font("Arial", 20));
        Label fileLabel1 = new Label("Font Style: ");
        fileLabel1.setFont(new Font("Arial", 20));
        Label fileLabel2 = new Label("Font Family: ");
        fileLabel2.setFont(new Font("Arial", 20));
        TextField op = new TextField("");
        ChoiceBox op1 = new ChoiceBox();
        ChoiceBox op2 = new ChoiceBox();
        FlowPane buttonSpot = new FlowPane();
        HBox po1 = new HBox();
        po1.getChildren().add(fileLabel1);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel2);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            uniFont = new Font("Times New Roman",15);
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();

       //@TODO set image implementation
    }

    public void createTextType(String x) {
        if(x.equals("Header"))
        {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Header:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.title*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cus = {"header",op.getText()};
	    champ.getSelectedPage().addComponent("text",cus,uniFont);
            ui.showPage();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        }
        else if(x.equals("Paragraph"))
        {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Paragraph:");
        fileLabel.setFont(new Font("Arial", 20));
         TextArea op = new TextArea("Text Sample");
        op.setPrefSize(200, 40);
        
        op.setOnContextMenuRequested(new EventHandler<Event>()
        {
            @Override
            public void handle(Event arg0)
            {
                selectedText = op.getSelectedText();
            }
        });

        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
         Button hyper = new Button("Add HyperLink");
        hyper.setOnAction(e -> {
            addHyperLink();
        });
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cus = {"paragraph",op.getText()};
	    champ.getSelectedPage().addComponent("text",cus,uniFont);
            ui.showPage();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(hyper);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        }
        else if(x.equals("List"))
        {
        int counts = 0;
        s1 = new ArrayList<String>();
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Items:");
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Add Item");
        op.setOnAction(e -> {
            addItem(counts,fileLabel);
            
        });   
        ChoiceBox itemListing = new ChoiceBox();
        Button remover = new Button("Remove");
        FlowPane buttonSpot = new FlowPane();
        VBox po = new VBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        HBox po2 = new HBox(3);
        po2.getChildren().add(remover);
        po2.getChildren().add(itemListing);
 
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            
            s1.add(0,"list");
            String[] sto = new String[s1.size()];
            sto = s1.toArray(sto);
            dia.close();
	    champ.getSelectedPage().addComponent("text",sto,uniFont);
            ui.showPage();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        //HBox fooo = new HBox(5);
        //fooo.getChildren().add(po);
        //fooo.getChildren().add(po2);
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po2);
        
        co.setPrefHeight(300);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        content.setPrefHeight(400);
        Scene gop = new Scene(content,400,300);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.setHeight(500);
        dia.setHeight(400);
        dia.showAndWait();
        }
    }

    public void addHyperLink() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Url:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.title*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            selectedUrl = op.getText();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
    }

    public void addItem(int num,Label ini) {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Item "+num+":");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField();
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
        po.getStyleClass().add("distort");
        //@TODO
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
    
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            
            dia.close();
            s1.add(op.getText());
            ini.setText(ini.getText()+op.getText()+"\n");
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }
    
    public void handleAddPortfolio() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        //Content
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        
        Button op = new Button("Select Portfolio");
        op.setTooltip(new Tooltip("Load a Portfolio"));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        
        
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        
        //Buttons
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            ui.pfc.loadPortfolio(champ);
            
        });
        
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        //ButtonPanel
        FlowPane buttonSpot = new FlowPane();
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        
        //ContentLayout
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set image implementation
    }
    public void handleSavePortfolio() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Enter file name to save as here:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.title*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
        
        
        VBox co = new VBox(10);
        co.getChildren().add(po);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            try{
            ui.pfc.saveAsPortfolio(op.getText(),ui.champ);
            }catch(Exception e1)
            {System.out.println("Issue 3");}
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        dia.showAndWait();
    }

    }
    
