/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import eportfoliogenerator.EPortfolioGeneratorView;

/**
 *
 * @author Thomas
 */
public class EPortfolioGenerator extends Application {
    
    EPortfolioGeneratorView ui = new EPortfolioGeneratorView();
    @Override
    public void start(Stage primaryStage) {
        ui.startUI();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
