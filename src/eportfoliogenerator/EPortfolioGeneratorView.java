/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

/**
 *
 * @author Thomas
 */
import static eportfoliogenerator.Constants.*;
import eportfoliogenerator.Contents.Component;
import eportfoliogenerator.Contents.Slide;
import eportfoliogenerator.Contents.SlideShowComponent;
import eportfoliogenerator.Contents.TextComponent;
import java.awt.Dimension;
import javafx.scene.text.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.*;
public class EPortfolioGeneratorView {
    
    Stage primaryStage;
    Scene primaryScene;
    BorderPane mainPane;
    HBox fileToolbar;
    HBox siteToolbar;
    FlowPane workSpaceModeToolbar;
    VBox editorToolbar;
    TabPane tPane;
    Tab pageEditWorkspace;
    Tab siteViewWorkspace;
    BorderPane pewPane;
    BorderPane svwPane;
    HBox footer;
    VBox pWorkspace;
    VBox sWorkspace;
    VBox toolbarPane;
    ArrayList<Slide> uniSlide;
    Button nP;
    Button lP;
    Button sP;
    Button sAP;
    Button eP;
    Button eX;
    
    Button switchMode;
    
    Label selectPage;
    ChoiceBox choosePage;
    Button nPage;
    Button rPage;
    Font uniFont;
    Button editName;
    Button selectLayout;
    Button selectColor;
    Button selectBannerImage;
    Button updateFooter;
    Button addTextComponent;
    Button addImageComponent;
    Button addVideoComponent;
    Button addSlideShowComponent;
    Button editSelectedComponent;
    Button removeSelectedComponent;
    Button updatePageTitle;
    ArrayList<String> s1 = new ArrayList<String>();
    Page selectedPage;
    ArrayList<Page> pageList = new ArrayList<Page>();
    ArrayList<Button> buttonList = new ArrayList<Button>();
    Component selectedComponent = null;
    String footerContent;
    String[] lay = {"A","B","C","D","E"};
    String[] col = {"A","B","C","D","E"};
    String[] textTypes = {"Header","Paragraph","List"};
    String username;
    File selectedFile;
   public EPortfolioGeneratorView()
   {      
       mainPane = new BorderPane();
       svwPane = new BorderPane();
       pewPane = new BorderPane();
       sWorkspace = new VBox();
       pWorkspace = new VBox(10);
       toolbarPane = new VBox();
       footer = new HBox();
       editorToolbar = new VBox(20);
       fileToolbar = new HBox(10);
       siteToolbar = new HBox(25);
       workSpaceModeToolbar = new FlowPane();
       pageEditWorkspace = new Tab();
       siteViewWorkspace = new Tab();
       tPane = new TabPane();
       this.setUserInterface();
       primaryScene = new Scene(mainPane);
       primaryScene.getStylesheets().add("eportfoliogenerator/style/style.css");
       primaryStage = new Stage();
       footerContent = "";
       primaryStage.setScene(primaryScene);
       primaryStage.setWidth(1600);
       primaryStage.setHeight(1000);
   }
   public void setEditorToolbar(){
       editName = initChildButton(editorToolbar, NAME_FN, NAME_FN_D,CSS_EDITOR_TOOLBAR,false);
       selectLayout = initChildButton(editorToolbar, LAYOUT_FN, LAYOUT_FN_D,CSS_EDITOR_TOOLBAR,false);
       selectColor = initChildButton(editorToolbar, COLOR_FN, COLOR_FN_D,CSS_EDITOR_TOOLBAR,false);
       selectBannerImage = initChildButton(editorToolbar, BANNER_FN, BANNER_FN_D,CSS_EDITOR_TOOLBAR,false);
       updateFooter = initChildButton(editorToolbar, FOOTER_FN, FOOTER_FN_D,CSS_EDITOR_TOOLBAR,false);
       addTextComponent = initChildButton(editorToolbar, TEXT_FN, TEXT_FN_D,CSS_EDITOR_TOOLBAR,false);
       addImageComponent = initChildButton(editorToolbar, IMG_FN, IMG_FN_D,CSS_EDITOR_TOOLBAR,false);
       addVideoComponent = initChildButton(editorToolbar, VID_FN, VID_FN_D,CSS_EDITOR_TOOLBAR,false);
       addSlideShowComponent = initChildButton(editorToolbar, SS_FN, SS_FN_D,CSS_EDITOR_TOOLBAR,false);
       editSelectedComponent = initChildButton(editorToolbar, EDIT_COMP, EDIT_COMP_D,CSS_EDITOR_TOOLBAR,false);
       removeSelectedComponent = initChildButton(editorToolbar, REMOVE_COMP, REMOVE_COMP_D,CSS_EDITOR_TOOLBAR,false);
       updatePageTitle = initChildButton(editorToolbar, PAGE_TITLE, PAGE_TITLE_D,CSS_EDITOR_TOOLBAR,false);
       siteToolbar.getStyleClass().add(CSS_EDITOR_TOOLBAR);
   }
   public void setFileToolbar(){
        nP = initChildButton(fileToolbar, NP_ICON, NP_ICON_D,CSS_FILE_TOOLBAR,false);
        lP = initChildButton(fileToolbar, LP_ICON, LP_ICON_D,CSS_FILE_TOOLBAR,false);
        sP = initChildButton(fileToolbar, SP_ICON, SP_ICON_D,CSS_FILE_TOOLBAR,false);
        sAP = initChildButton(fileToolbar, SAP_ICON, SAP_ICON_D,CSS_FILE_TOOLBAR,false);
        eP = initChildButton(fileToolbar, EP_ICON, EP_ICON_D,CSS_FILE_TOOLBAR,false);
        eX = initChildButton(fileToolbar, EXIT_ICON, EXIT_ICON_D,CSS_FILE_TOOLBAR,false);
        fileToolbar.getStyleClass().add(CSS_FILE_TOOLBAR);
   }
   public void setSiteToolbar(){
       selectPage = new Label("Select Page To Edit:");
       selectPage.setFont(new Font("Arial",25));
       ObservableList<String> sammy = FXCollections.<String>observableArrayList();
       for (Page pageList1 : pageList) {
            sammy.add(pageList1.getPageTitle());
        }
       choosePage = new ChoiceBox(sammy);
       choosePage.setTooltip(new Tooltip("Select Page"));
       nPage = initChildButton(siteToolbar, NPAGE_ICON, NPAGE_ICON_D,CSS_SITE_TOOLBAR,false);
       nPage.setStyle("-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 )");
       
       rPage = initChildButton(siteToolbar, RPAGE_ICON, RPAGE_ICON_D,CSS_SITE_TOOLBAR,false);
       rPage.setStyle("-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 )");
       siteToolbar.getChildren().add(selectPage);
       siteToolbar.getChildren().add(choosePage);
       siteToolbar.getStyleClass().add(CSS_SITE_TOOLBAR);
   }
public Button initChildButton(Pane toolbar, String iconFileName, String desc, String cssClass, boolean disabled) {
    String imagePath = "file:" + ICON_PATH + iconFileName;
    Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(desc);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
}
public void initEventHandlers()
{
    FileController fileMngr = new FileController();
    editName.setOnAction(e -> {
	    handleName();
	});
    selectLayout.setOnAction(e -> {
	    handleLayout();
	});
    selectColor.setOnAction(e -> {
	    handleColor();
	});
    selectBannerImage.setOnAction(e -> {
	    handleBanner();
	});
    updateFooter.setOnAction(e -> {
	    handleFooter();
	});
    addTextComponent.setOnAction(e -> {
	    handleAddText();
	});
    addImageComponent.setOnAction(e -> {
	    handleAddImage();
	});
    addVideoComponent.setOnAction(e -> {
	    handleAddVideo();
	});
    addSlideShowComponent.setOnAction(e -> {
	    handleAddSS();
	});
    editSelectedComponent.setOnAction(e -> {
	    handleEdit();
	});
    removeSelectedComponent.setOnAction(e -> {
	    handleRemove();
	});
    updatePageTitle.setOnAction(e -> {
	    handleTitle();
	});
    nPage.setOnAction(e -> {
	    addPage();
	});
}
private void setUserInterface()
{
    svwPane.setCenter(sWorkspace);
    pewPane.setCenter(pWorkspace);
    pewPane.getStyleClass().add("PEW");
    svwPane.setBottom(footer);
    pewPane.setBottom(footer);
    pewPane.setLeft(editorToolbar);
    setEditorToolbar();
    pageEditWorkspace.setContent(pewPane);
    siteViewWorkspace.setContent(svwPane);
    pageEditWorkspace.setText("Page Edit Workspace");
    siteViewWorkspace.setText("Site View Workspace");
    tPane.getTabs().add(pageEditWorkspace);
    tPane.getTabs().add(siteViewWorkspace);
    tPane.getStyleClass().add("site_toolbar");
    siteViewWorkspace.getStyleClass().add("site_toolbar");
    pageEditWorkspace.getStyleClass().add("site_toolbar");
    toolbarPane.getChildren().add(fileToolbar);
    toolbarPane.getChildren().add(siteToolbar);
    toolbarPane.getChildren().add(workSpaceModeToolbar);
    setFileToolbar();
    setSiteToolbar();
    initEventHandlers();
    mainPane.setTop(toolbarPane);
    mainPane.setCenter(tPane);
}
public void loadPage(Page currentPage)
{
    
}
public void startUI()
{
    primaryStage.show();
}
public void setPEWComp()
{
    pWorkspace.getChildren().clear();
    pWorkspace.setAlignment(Pos.CENTER);
    buttonList = new ArrayList<Button>();
    for(int i = 0; i < selectedPage.content.size();i++ )
    {
        Component tempCo = selectedPage.content.get(i);
        Button var = new Button(tempCo.type+"");
        var.setPrefHeight(150);
        var.setPrefWidth(300);
        var.setOnAction(e -> {
	    setSelectedComponent(tempCo);
	});
        buttonList.add(var);
        pWorkspace.getChildren().add(buttonList.get(i));
    }
    setSelectedComponent(selectedComponent);
}
/*
===========================================================================================================================================
===========================================================================================================================================
===========================================================================================================================================
===========================================================================================================================================
==================================================================EVENTHANDLERS============================================================
===========================================================================================================================================
===========================================================================================================================================
===========================================================================================================================================
===========================================================================================================================================
*/
private void addPage()
{
    Page newpage = new Page();
    selectedPage = newpage;
    pageList.add(newpage);
    newpage.footer = "";
}
private void handleName() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        Label fileLabel = new Label("Profile Name:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(selectedPage.title);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            username = op.getText();
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getChildren().add(fontChanger);//
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }
    private void handleLayout() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        
        Label op = new Label("Select Layout:");
        op.setFont(new Font("Arial", 20));
        ObservableList<String> lChoiceList = FXCollections.<String>observableArrayList();
        lChoiceList.addAll(Arrays.asList(lay));
        ChoiceBox layoutChoices = new ChoiceBox(lChoiceList);
        HBox buttonSpot = new HBox(5);
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(layoutChoices);
        po.getStyleClass().add("husk");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            selectedPage.layout = op.getText();
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        bOK.setAlignment(Pos.BOTTOM_CENTER);
        bCancel.setAlignment(Pos.BOTTOM_CENTER);
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        //@TODO : set bOK activation to implement layoutChoices, set bCancel to cancel
    }

    private void handleColor() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        Label op = new Label("Select Color:");
        
        op.setFont(new Font("Arial", 20));
        ObservableList<String> lChoiceList = FXCollections.<String>observableArrayList();
        lChoiceList.addAll(Arrays.asList(col));
        ChoiceBox colorChoices = new ChoiceBox(lChoiceList);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(colorChoices);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            selectedPage.color = op.getText();
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        //@TODO : set bOK activation to implement colorChoices, set bCancel to cancel
    }

    private void handleBanner() {
         Stage dia = new Stage();
         dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Select Banner Image");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            selectedPage.banner = selectedFile;
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        dia.showAndWait();
       //@TODO set image implementation
    }

    private void handleFooter() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        Label fileLabel = new Label("Footer:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.footer*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            selectedPage.footer = op.getText();
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        BorderPane content = new BorderPane();
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getChildren().add(fontChanger);//
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }

    private void handleAddText() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label op = new Label("Select Text Type:");
        op.setFont(new Font("Arial", 20));
        ObservableList<String> lChoiceList = FXCollections.<String>observableArrayList();
        lChoiceList.addAll(Arrays.asList(textTypes));
        ChoiceBox colorChoices = new ChoiceBox(lChoiceList);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(colorChoices);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            createTextType(colorChoices.getValue().toString());
            
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.show();
    }

    private void handleAddImage() {
        Stage dia = new Stage();
        dia.setHeight(300);
        dia.setWidth(400);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Select Image");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        Label filesLabel = new Label("Caption:");
        filesLabel.setFont(new Font("Arial", 20));
        TextField op1 = new TextField();
        Label fileLabel1 = new Label("Height:");
        fileLabel1.setFont(new Font("Arial", 15));
        TextField op2 = new TextField();
        Label fileLabel2 = new Label("Width:");
        fileLabel2.setFont(new Font("Arial", 15));
        TextField op3 = new TextField();
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel1);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        HBox po3 = new HBox();
        po3.getChildren().add(fileLabel2);
        po3.getChildren().add(op3);
        po3.getStyleClass().add("distort");
        HBox po1 = new HBox();
        po1.getChildren().add(filesLabel);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cla = {"image", selectedFile.getName(),op1.getText(),op2.getText(),op3.getText()};
            selectedPage.addComponent("image",cla,uniFont);
           setPEWComp(); 
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        Button fontChanger = new Button("Set Font");
        //@TODO
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        fontChanger.setOnAction(e -> {
            setFont();
        });
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set image implementation
    }

    private void handleAddVideo() {
        Stage dia = new Stage();
        dia.setHeight(300);
        dia.setWidth(400);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Select Video");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        Label filesLabel = new Label("Caption:");
        filesLabel.setFont(new Font("Arial", 20));
        TextField op1 = new TextField();
        Label fileLabel1 = new Label("Height:");
        fileLabel1.setFont(new Font("Arial", 15));
        TextField op2 = new TextField();
        Label fileLabel2 = new Label("Width:");
        fileLabel2.setFont(new Font("Arial", 15));
        TextField op3 = new TextField();
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel1);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        HBox po3 = new HBox();
        po3.getChildren().add(fileLabel2);
        po3.getChildren().add(op3);
        po3.getStyleClass().add("distort");
        HBox po1 = new HBox();
        po1.getChildren().add(filesLabel);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cla = {"video", selectedFile.getName(),op1.getText(),op2.getText(),op3.getText()};
            selectedPage.addComponent("video",cla,uniFont);
            setPEWComp();
            
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        Button fontChanger = new Button("Set Font");
        //@TODO
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        fontChanger.setOnAction(e -> {
            setFont();
        });
        bCancel.setOnAction(e -> {
            dia.close();
        });
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set image implementation
    }

    private void handleAddSS() {
        Stage dia = new Stage();
        uniSlide = new ArrayList<Slide>(0);
        uniFont = new Font("Times New Roman", 15);
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label titleLabel = new Label("Enter Slideshow Title:");
        titleLabel.setFont(new Font("Arial", 20));
        TextField titleAcceptor = new TextField();
        Label fileLabel = new Label("Slides created:");
        fileLabel.setFont(new Font("Arial", 20));
        Label filesLabel = new Label("");
        filesLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Add Slide");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    addSlide(fileLabel,dia,filesLabel);
	});
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(titleLabel);
        po.getChildren().add(titleAcceptor);
        po.getStyleClass().add("distort");
        //@TODO
        VBox po1 = new VBox();
        po1.getChildren().add(op);
        po1.getChildren().add(fileLabel);
        po1.getChildren().add(filesLabel);
        po1.getStyleClass().add("distort");
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            String[] cla = {"slideshow", titleAcceptor.getText()};
            selectedPage.content.add(new SlideShowComponent("slideshow",cla,uniSlide,uniFont));
            setPEWComp();
            dia.close();
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set image implementation
    }

    private void handleEdit() {
        if(selectedComponent.type.equals("text"))
        {
            String x = selectedComponent.content[0];
            uniFont = selectedComponent.font;
            if(x.equals("header"))
        {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Header:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(selectedComponent.content[1]);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cus = {"header",op.getText()};
	    selectedComponent.content = cus;
            selectedComponent.font = uniFont;
            setPEWComp();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        }
        else if(x.equals("paragraph"))
        {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Paragraph:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(selectedComponent.content[1]);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
         Button hyper = new Button("Add HyperLink");
        hyper.setOnAction(e -> {
            addHyperLink();
        });
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cus = {"paragraph",op.getText()};
	    selectedComponent.content = cus;
            selectedComponent.font = uniFont;
            setPEWComp();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(hyper);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        }
        else if(x.equals("list"))
        {
        int counts = 0;
        s1 = new ArrayList<String>();
        String[] pop = selectedComponent.content;
        for(String uio: pop)
            s1.add(uio);
        s1.remove(0);
        String listOf = "";
        for(int y = 0; y < s1.size();y++)
            listOf += s1.get(y)+"\n";
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Items:"+ listOf);
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Add Item");
        op.setOnAction(e -> {
            addItem(counts,fileLabel);
            
        });   
        ChoiceBox itemListing = new ChoiceBox();
        Button remover = new Button("Remove");
        FlowPane buttonSpot = new FlowPane();
        VBox po = new VBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        HBox po2 = new HBox(3);
        po2.getChildren().add(remover);
        po2.getChildren().add(itemListing);
 
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            
            s1.add(0,"list");
            String[] sto = new String[s1.size()];
            sto = s1.toArray(sto);
            dia.close();
	    selectedComponent.content = sto;
            selectedComponent.font = uniFont;
            setPEWComp();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        //HBox fooo = new HBox(5);
        //fooo.getChildren().add(po);
        //fooo.getChildren().add(po2);
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po2);
        
        co.setPrefHeight(300);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        content.setPrefHeight(400);
        Scene gop = new Scene(content,400,300);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.setHeight(500);
        dia.setHeight(400);
        dia.showAndWait();
        }
        }
        else if(selectedComponent.type.equals("image"))
        {
            uniFont = selectedComponent.font;
            Stage dia = new Stage();
        dia.setHeight(300);
        dia.setWidth(400);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Selected File: " +  selectedComponent.content[1]);
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Select Image");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
	});
        Label filesLabel = new Label("Caption:");
        filesLabel.setFont(new Font("Arial", 20));
        TextField op1 = new TextField(selectedComponent.content[2]);
        Label fileLabel1 = new Label("Height:" );
        fileLabel1.setFont(new Font("Arial", 15));
        TextField op2 = new TextField(selectedComponent.content[3]);
        Label fileLabel2 = new Label("Width:");
        fileLabel2.setFont(new Font("Arial", 15));
        TextField op3 = new TextField(selectedComponent.content[4]);
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel1);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        HBox po3 = new HBox();
        po3.getChildren().add(fileLabel2);
        po3.getChildren().add(op3);
        po3.getStyleClass().add("distort");
        HBox po1 = new HBox();
        po1.getChildren().add(filesLabel);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cla = {"image", selectedFile.getName(),op1.getText(),op2.getText(),op3.getText()};
            selectedComponent.content = cla;
           setPEWComp(); 
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        Button fontChanger = new Button("Set Font");
        //@TODO
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        fontChanger.setOnAction(e -> {
            setFont();
        });
        bCancel.setOnAction(e -> {
            dia.close();
        });
        
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set image implementation
        }
    }

    private void handleRemove() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Remove Selected Component?");
        fileLabel.setFont(new Font("Arial", 20));
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
         po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        content.setCenter(po);
        content.setBottom(buttonSpot);
        content.getStyleClass().add("small");
       Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }

    private void handleTitle() {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Page Title:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.title*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
        Button fontChanger = new Button("Set Font");
        
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        bCancel.setOnAction(e -> {
            dia.close();
        });
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }

    private void setFiles(Label unknowing,Stage known) {
        FileChooser slideShowFileChooser = new FileChooser();
        selectedFile = slideShowFileChooser.showOpenDialog(known);
        unknowing.setText(selectedFile.getName());
    }
    private void setFilesSlide(Label unknowing,Stage known,Label beq) {
        FileChooser slideShowFileChooser = new FileChooser();
        selectedFile = slideShowFileChooser.showOpenDialog(known);
        
        
    }
    public void addSlide(Label unknowing,Stage known,Label beq)
    {
        Stage dia = new Stage();
        dia.setHeight(300);
        dia.setWidth(400);
        selectedFile = null;
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Selected File: null");
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Select Image");
        op.setTooltip(new Tooltip(BANNER_FN_D));
        op.setOnAction(e -> {
	    setFiles(fileLabel,dia);
            beq.setText(beq.getText() +"\n"+selectedFile.getName());
	});
        Label filesLabel = new Label("Caption:");
        filesLabel.setFont(new Font("Arial", 20));
        TextField op1 = new TextField();
        FlowPane buttonSpot = new FlowPane();
        Label fileLabel1 = new Label("Height:");
        fileLabel1.setFont(new Font("Arial", 15));
        TextField op2 = new TextField();
        Label fileLabel2 = new Label("Width:");
        fileLabel2.setFont(new Font("Arial", 15));
        TextField op3 = new TextField();
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel1);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        HBox po3 = new HBox();
        po3.getChildren().add(fileLabel2);
        po3.getChildren().add(op3);
        po3.getStyleClass().add("distort");
        HBox po1 = new HBox();
        po1.getChildren().add(filesLabel);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        HBox po = new HBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            uniSlide.add(new Slide(selectedFile.getName(),op1.getText(),op2.getText(),op3.getText(),uniFont));
        });
        Button bCancel = new Button("Cancel");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        co.getChildren().add(po3);
        co.getStyleClass().add("distort");
        co.getChildren().add(fontChanger);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
       //@TODO set image implementation
    }
    public void setFont()
    {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Font Size: ");
        fileLabel.setFont(new Font("Arial", 20));
        Label fileLabel1 = new Label("Font Style: ");
        fileLabel1.setFont(new Font("Arial", 20));
        Label fileLabel2 = new Label("Font Family: ");
        fileLabel2.setFont(new Font("Arial", 20));
        TextField op = new TextField("");
        ChoiceBox op1 = new ChoiceBox();
        ChoiceBox op2 = new ChoiceBox();
        FlowPane buttonSpot = new FlowPane();
        HBox po1 = new HBox();
        po1.getChildren().add(fileLabel1);
        po1.getChildren().add(op1);
        po1.getStyleClass().add("distort");
        HBox po2 = new HBox();
        po2.getChildren().add(fileLabel2);
        po2.getChildren().add(op2);
        po2.getStyleClass().add("distort");
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
        po.getStyleClass().add("distort");
        //@TODO
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            uniFont = new Font("Times New Roman",15);
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po1);
        co.getChildren().add(po2);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();

       //@TODO set image implementation
    }

    private void createTextType(String x) {
        if(x.equals("Header"))
        {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Header:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.title*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cus = {"header",op.getText()};
	    selectedPage.addComponent("text",cus,uniFont);
            setPEWComp();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        }
        else if(x.equals("Paragraph"))
        {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Paragraph:");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField(/*seletedPage.title*/);
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
         po.getStyleClass().add("distort");
        //@TODO
         Button hyper = new Button("Add HyperLink");
        hyper.setOnAction(e -> {
            addHyperLink();
        });
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            dia.close();
            String[] cus = {"paragraph",op.getText()};
	    selectedPage.addComponent("text",cus,uniFont);
            setPEWComp();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(hyper);
        co.getChildren().add(fontChanger);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.showAndWait();
        }
        else if(x.equals("List"))
        {
        int counts = 0;
        s1 = new ArrayList<String>();
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Items:");
        fileLabel.setFont(new Font("Arial", 20));
        Button op = new Button("Add Item");
        op.setOnAction(e -> {
            addItem(counts,fileLabel);
            
        });   
        ChoiceBox itemListing = new ChoiceBox();
        Button remover = new Button("Remove");
        FlowPane buttonSpot = new FlowPane();
        VBox po = new VBox();
        po.getChildren().add(op);
        po.getChildren().add(fileLabel);
        po.getStyleClass().add("distort");
        HBox po2 = new HBox(3);
        po2.getChildren().add(remover);
        po2.getChildren().add(itemListing);
 
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            
            s1.add(0,"list");
            String[] sto = new String[s1.size()];
            sto = s1.toArray(sto);
            dia.close();
	    selectedPage.addComponent("text",sto,uniFont);
            setPEWComp();
	});
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        bCancel.setOnAction(e -> {
            dia.close();
        });
        //HBox fooo = new HBox(5);
        //fooo.getChildren().add(po);
        //fooo.getChildren().add(po2);
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(po2);
        
        co.setPrefHeight(300);
        co.getStyleClass().add("distort");
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        content.setPrefHeight(400);
        Scene gop = new Scene(content,400,300);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        dia.setHeight(500);
        dia.setHeight(400);
        dia.showAndWait();
        }
    }

    private void addHyperLink() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void addItem(int num,Label ini) {
        Stage dia = new Stage();
        dia.setHeight(200);
        dia.setWidth(300);
        BorderPane content = new BorderPane();
        Label fileLabel = new Label("Item "+num+":");
        fileLabel.setFont(new Font("Arial", 20));
        TextField op = new TextField();
        FlowPane buttonSpot = new FlowPane();
        HBox po = new HBox();
        po.getChildren().add(fileLabel);
        po.getChildren().add(op);
        po.getStyleClass().add("distort");
        //@TODO
         Button fontChanger = new Button("Set Font");
        fontChanger.setOnAction(e -> {
            setFont();
        });
    
        Button bOK = new Button("OK");
        bOK.setOnAction(e -> {
            
            dia.close();
            s1.add(op.getText());
            ini.setText(ini.getText()+op.getText()+"\n");
        });
        Button bCancel = new Button("Cancel");
        buttonSpot.getChildren().add(bOK);
        buttonSpot.getChildren().add(bCancel);
        buttonSpot.getStyleClass().add("confirm");
        //@TODO
        bCancel.setOnAction(e -> {
            dia.close();
        });
        VBox co = new VBox(10);
        co.getChildren().add(po);
        co.getChildren().add(fontChanger);
        content.setCenter(co);
        content.setBottom(buttonSpot);
        
        content.getStyleClass().add("small");
        Scene gop = new Scene(content);
        gop.getStylesheets().add("eportfoliogenerator/style/style.css");
        dia.setScene(gop);
        
        dia.showAndWait();
       //@TODO set bOK action to change footer
    }

    private void setSelectedComponent(Component get) {
        selectedComponent = get;
        for(int i = 0;i<selectedPage.content.size(); i++)
        {
            if(selectedPage.content.get(i).equals(selectedComponent))
                buttonList.get(i).setStyle("-fx-background-color: ORANGE;");
            else
                buttonList.get(i).setStyle("-fx-background-color: GRAY;");
        }
    }

}
// addSS
//addText
//Page Icon
//Color
//distort style
//small style
//confirm
//button and toolbar styles
